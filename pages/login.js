import react, { Component } from "react"
import styled from "styled-components"
import { Formik } from "formik"
import { UsernameIcon, PasswordIcon } from "../components/app/Icons"
import Loading from "../components/app/loading"
import Link from "next/link"
import { postData } from "../utils/main"
import Router from "next/router"

export default class Login extends Component {
  state = {
    firstTime: 0,
    msgstatus: "none",
    msgcolor: "#fff",
    msgtext: " "
  }

  fistTimeCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("firsttime") === null) {
        Router.push("/landing")
      } else {
        this.setState({
          firstTime: 1
        })
      }
    }
  }

  componentDidMount() {
    this.fistTimeCheck()
  }

  render = () => (
    <>
      {this.state.firstTime == 0 ? (
        <Loading />
      ) : (
        <div>
          <LoginBox>
            <LoginLogo>
              <img src="/static/images/final-blue.png" />
            </LoginLogo>
            <Formik
              initialValues={{ email: "", password: "" }}
              // validate={values => {
              //   let errors = {}
              //   if (!values.email) {
              //     errors.email = "Required"
              //   } else if (
              //     !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
              //   ) {
              //     errors.email = "Invalid email address"
              //   }
              //   return errors
              // }}
              onSubmit={(values, { setSubmitting }) => {
                setTimeout(() => {
                  postData(
                    "https://api.mrezak.ir/update.php?action=login",
                    values
                  ).then(data => {
                    if (data[0] == "logged") {
                      window.localStorage.setItem("usertoken", data[1])
                      Router.push("/dashboard")
                    } else {
                      console.log(data)
                    }
                  })
                  setSubmitting(false)
                }, 400)
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting
                /* and other goodies */
              }) => (
                <LoginForm onSubmit={handleSubmit}>
                  <LogItemContainer>
                    <UsernameIcon />
                    <input
                      type="email"
                      name="email"
                      placeholder="Email"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                    />
                    {/* {errors.email && touched.email && errors.email} */}
                  </LogItemContainer>
                  <LogItemContainer>
                    <PasswordIcon />
                    <input
                      type="password"
                      name="password"
                      placeholder="Password"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.password}
                    />
                    {/* {errors.password && touched.password && errors.password} */}
                  </LogItemContainer>
                  <LoginBTN type="submit" disabled={isSubmitting}>
                    Submit
                  </LoginBTN>
                </LoginForm>
              )}
            </Formik>
            <ForgetPass>
              <Link href="#">
                <a>Forgot Password?</a>
              </Link>
              <Link href="/signup">
                <a>Don't have an account?</a>
              </Link>
            </ForgetPass>
            <LoginMsgBox
              msgstatus={this.state.msgstatus}
              msgcolor={this.state.msgcolor}
            >
              <p>{this.state.msgtext}</p>
            </LoginMsgBox>
          </LoginBox>
        </div>
      )}
    </>
  )
}

const LoginBox = styled.div`
  width: 85%;
  height: 450px;
  margin: 50px auto;
  overflow: hidden;
`
const LoginLogo = styled.div`
  width: 100px;
  height: 100px;
  margin: 0px auto;
  border-radius: 50px;
  img {
    display: block;
    width: 100px;
    height: 100px;
  }
`
const LoginForm = styled.form`
  width: 100%;
  height: auto;
  overflow: hidden;
  span {
    display: block;
    width: 100%;
    height: 30px;
  }
`
const LogItemContainer = styled.div`
  width: 90%;
  height: 40px;
  border-bottom: 1px solid #153e79;
  margin: 20px auto;
  svg {
    width: 20px;
    height: 20px;
    margin-left: 5px;
    margin-top: 10px;
  }
  input {
    width: 87%;
    padding-left: 5px;
    margin-top: 5px;
    height: 30px;
    float: right;
    background: none;
    border: medium none;
    font-size: 15px;
    font-family: Montserrat;
  }
  input::placeholder {
    color: #153e79;
    opacity: 1;
    font-size: 12px;
  }
`
const LoginBTN = styled.button`
  width: 150px;
  height: 40px;
  display: block;
  background: #153e79;
  border-radius: 10px;
  border: medium none;
  text-align: center;
  line-height: 40px;
  color: #fff;
  font-family: Montserrat;
  margin: 40px auto;
  cursor: pointer;
`
const ForgetPass = styled.div`
  width: 200px;
  height: 30px;
  text-align: center;
  line-height: 30px;
  margin: 0px auto;
  a {
    display: block;
    width: 100%;
    height: 40px;
    font-size: 13px;
    text-decoration: none;
    color: #153e79;
    font-family: Montserrat;
  }
`
const LoginMsgBox = styled.div`
  width: 200px;
  height: 40px;
  background: ${p => (p.msgcolor ? p.msgcolor : "#fff")};
  display: ${d => (d.msgstatus ? d.msgstatus : "block")};
  margin: 40px auto;
  text-align: center;
  line-height: 40px;
  color: #fff;
  border-radius: 5px;
`
