import PanelLayout from "../components/app/PanelLayout"
import styled from "styled-components"
import { Component } from "react"
import Loading from "../components/app/loading"
import { Navbar, NavBlank } from "../components/app/Navbar"
import { NewGroup, NewGroupBlank } from "../components/app/NewGroup"
import { AddIcon, BarsIcon } from "../components/app/Icons"
import { postData } from "../utils/main"
import Router from "next/router"

export default class Dashboard extends Component {
  state = {
    logged: 0,
    NavBarLeft: -230,
    NavBarDisplay: "none",
    NewGroupTop: -230,
    NewGroupDisplay: "none"
  }

  loginCheck = () => {
    if (typeof window == "object") {
      if (window.localStorage.getItem("usertoken") === null) {
        Router.push("/login")
      } else {
        const userTk = window.localStorage.getItem("usertoken")
        postData(
          "https://api.mrezak.ir/update.php?action=tokencheck&token=" + userTk
        ).then(data => {
          if (data == "good") {
            this.setState({
              logged: 1
            })
          } else {
            Router.push("/login")
          }
        })
      }
    }
  }

  handleNavClick = () => {
    this.setState({
      NavBarLeft: 0,
      NavBarDisplay: "block"
    })
  }

  handleNavCloser = () => {
    this.setState({
      NavBarLeft: -230,
      NavBarDisplay: "none"
    })
  }

  handleNewGroupClick = () => {
    this.setState({
      NewGroupTop: 100,
      NavBarDisplay: "block"
    })
  }

  handleNewGroupCloser = () => {
    this.setState({
      NewGroupTop: -230,
      NewGroupDisplay: "none"
    })
  }

  componentDidMount() {
    this.loginCheck()
  }

  render = () => (
    <>
      {this.state.logged == 0 ? (
        <Loading />
      ) : (
        <PanelLayout>
          <DashboardWrapper>
            <NewGroup top={this.state.NewGroupTop} />
            <NewGroupBlank
              display={this.state.NewGroupDisplay}
              onClick={this.state.NewGroupDisplay}
            />
            <Navbar left={this.state.NavBarLeft} />
            <NavBlank
              display={this.state.NavBarDisplay}
              onClick={this.handleNavCloser}
            />
            <DashSearchContainer>
              <span>
                <BarsIcon onClick={this.handleNavClick} />
              </span>
              <input type="text" placeholder="Search your notes" />
            </DashSearchContainer>
            <p align="center">Nothing to show yet</p>
            <AddBtnContainer>
              <AddIcon />
            </AddBtnContainer>
          </DashboardWrapper>
        </PanelLayout>
      )}
    </>
  )
}

const DashboardWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
  p {
    font-family: Montserrat;
    font-size: 13px;
    color: #999;
  }
`
const DashSearchContainer = styled.div`
  width: 85%;
  height: 36px;
  margin: 10px auto;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
  border: 2px solid #999;
  border-left: none;
  input {
    width: 80%;
    height: 34px;
    outline: medium none;
    border: 0;
    border-radius: 10px;
    display: block;
    padding-left: 5px;
    font-family: Montserrat;
    font-size: 13px;
    float: left;
  }
  span {
    width: 30px;
    height: 30px;
    float: left;
    margin-top: 5px;
    cursor: pointer;
  }
`
const AddBtnContainer = styled.div`
  width: 44px;
  height: 44px;
  border-radius: 22px;
  position: absolute;
  bottom: 20px;
  right: 20px;
  background: #eaeaea;
  cursor: pointer;
  svg {
    display: block;
    margin: 7px auto;
  }
`
