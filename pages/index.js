import Layout from "../components/main/MyLayout"
import Link from "next/link"
import styled from "styled-components"

const Index = () => (
  <Layout>
    <IndexWrapper>
      <p align="center">Hello, Titask is here</p>
      <Link href="/dashboard">
        <DashboardBTN>Go to Dashboard</DashboardBTN>
      </Link>
    </IndexWrapper>
  </Layout>
)

export default Index

const IndexWrapper = styled.div`
  width: 100%;
  height: auto;
  overflow: hidden;
`
const DashboardBTN = styled.button`
  width: 150px;
  height: 46px;
  display: block;
  background: #153e79;
  color: #fff;
  border-radius: 10px;
  border: medium none;
  box-shadow: 0px 0px 5px #153e79;
  margin: 0px auto;
  cursor: pointer;
`
