import React, { Component } from "react"
import styled from "styled-components"
import dynamic from "next/dynamic"
import Link from "next/link"
const Flickity = dynamic(import("react-flickity-component"), { ssr: false })

const flickityOptions = {
  cellAlign: "center",
  draggable: true,
  prevNextButtons: false,
  pageDots: true,
  freeScroll: false,
  cellAlign: "center"
}

export default class Landing extends Component {
  state = {
    images: [
      "/static/images/landing/0.png",
      "/static/images/landing/2.png",
      "/static/images/landing/3.png"
    ],
    texts: ["Being on schedule", "Stay away from deadlines", "Time management"]
  }

  setFirstTime = () => {
    window.localStorage.setItem("firsttime", 1)
  }

  render = () => (
    <LandingWrapper>
      <div class="landing-body">
        <Flickity options={flickityOptions}>
          <LandingItem>
            <LandingImage src={this.state.images[0]}></LandingImage>
            <LandingText>{this.state.texts[0]}</LandingText>
          </LandingItem>
          <LandingItem>
            <LandingImage src={this.state.images[1]}></LandingImage>
            <LandingText>{this.state.texts[1]}</LandingText>
          </LandingItem>
          <LandingItem>
            <LandingImage src={this.state.images[2]}></LandingImage>
            <LandingText>{this.state.texts[2]}</LandingText>
          </LandingItem>
        </Flickity>
      </div>
      <Link href="/login">
        <LandingSkip onClick={this.setFirstTime}>Done</LandingSkip>
      </Link>
    </LandingWrapper>
  )
}

/* - - - Page Components - - - */

const LandingWrapper = styled.div`
  width: 100%;
  height: 500px;
  div[class="landing-body"] {
    width: 100%;
    height: 480px;
    overflow: hidden;
    div {
      width: 100% !important;
      margin: 0px auto !important;
    }
    ol {
      width: 100% !important;
      margin: 0px auto !important;
      display: flex;
      padding: 0;
      justify-content: center;
      list-style: none;
      li {
        width: 10% !important;
        position: relative;
      }
      li::after {
        content: " ";
        width: 12px;
        height: 12px;
        border-radius: 6px;
        background: #fff;
        box-shadow: 0px 0px 3px #333;
        position: absolute;
        right: 0;
        left: 0;
        margin-right: auto;
        margin-left: auto;
        cursor: pointer;
      }
    }
  }
`

const LandingItem = styled.div`
  width: 100%;
  height: 450px;
  scroll-snap-align: start;
  overflow: hidden;
  margin-right: 10px;
  margin-left: 10px;
`
const LandingImage = styled.img`
  display: block;
  width: 300px;
  height: 300px;
  margin: 0px auto;
`
const LandingText = styled.span`
  display: block;
  width: 250px;
  height: 200px;
  margin: 0px auto;
  border-radius: 5px;
  text-align: center;
  color: #666;
  font-family: Montserrat;
  cursor: default;
`
const LandingSkip = styled.p`
  width: 100%;
  text-align: center;
  height: 50px;
  color: #153e79;
  font-family: Montserrat;
  cursor: pointer;
`
