import react, { Component } from "react"
import styled from "styled-components"
import { ViewModeIcon, NotifiIcon } from "./Icons"

export class Header extends Component {
  state = {}

  render = () => (
    <HeaderWrapper>
      <HeaderNotifi>
        <NotifiIcon />
      </HeaderNotifi>
      <HeaderTitle>
        <p align="center">Ttask</p>
      </HeaderTitle>
      <HeaderViewMode>
        <ViewModeIcon />
      </HeaderViewMode>
    </HeaderWrapper>
  )
}

/* - - - Page Components - - - */

const HeaderWrapper = styled.div`
  width: 100%;
  height: 45px;
  background: #153e79;
  display: flex;
  justify-content: space-between;
`
const HeaderTitle = styled.div`
  width: 40%;
  height: 45px;
  line-height: 50px;
  text-align: center;
  p {
    margin: 0;
    color: white;
    font-family: Montserrat;
  }
`
const HeaderViewMode = styled.div`
  width: 20%;
  height: 45px;
  svg {
    display: block;
    margin: 0px auto;
  }
`
const HeaderNotifi = styled.div`
  width: 20%;
  height: 45px;
  svg {
    display: block;
    margin: 7px auto;
  }
`
