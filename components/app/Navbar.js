import styled from "styled-components"
import Link from "next/link"
import * as Icons from "./Icons"

export const Navbar = props => {
  return (
    <NavWraper left={props.left}>
      <HeaderNavbar>
        <ProfilePicNavbar />
        <ProfileNameHeader>Mr/Miss User</ProfileNameHeader>
        <ProfileEmailHeader>user@gmail.com</ProfileEmailHeader>
      </HeaderNavbar>
      <RawNavbar>
        <ul>
          <Link href="#">
            <li>
              <Icons.NewGroupIcon />
              <a>New Group</a>
            </li>
          </Link>
          <Link href="#">
            <li>
              <Icons.GroupIcon />
              <a>Meetings</a>
            </li>
          </Link>
          <Link href="#">
            <li>
              <Icons.GroupIcon />
              <a>Birthdays</a>
            </li>
          </Link>
          <Link href="#">
            <li class="line">
              <Icons.GroupIcon />
              <a>Shopings</a>
            </li>
          </Link>
          <Link href="#">
            <li>
              <Icons.CalenderIcon />
              <a>Calender</a>
            </li>
          </Link>
          <Link href="#">
            <li>
              <RawNavImgbar src="/static/images/notification.png" />
              <a>Notification</a>
            </li>
          </Link>
          <Link>
            <li>
              <Icons.ThemeIcon />
              <a>Theme</a>
            </li>
          </Link>
          <Link href="#">
            <li>
              <Icons.SettingIcon />
              <a>Setting</a>
            </li>
          </Link>
        </ul>
      </RawNavbar>
    </NavWraper>
  )
}

export const NavBlank = props => {
  return <MainNavBlank display={props.display} {...props} />
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const NavWraper = styled.div`
  width: 230px;
  height: 100%;
  position: fixed;
  top: 0;
  bottom: 0;
  left: ${p => (p.left ? p.left : 0)}px;
  z-index: 2;
  background: #fff;
  font-family: Montserrat;
  transition: all 350ms;
`

const MainNavBlank = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  z-index: 1;
  top: 0;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 350ms;
`

/* - - - Header Navbar Components - - - */

const HeaderNavbar = styled.div`
  width: 100%;
  height: 120px;
  overflow: hidden;
  border-bottom: 1px solid #999;
`
const ProfilePicNavbar = styled.div`
  width: 60px;
  height: 60px;
  border-radius: 30px;
  background: #153e79;
  margin-top: 10px;
  margin-left: 10px;
`
const ProfileNameHeader = styled.span`
  width: 100%;
  height: 20px;
  font-size: 14px;
  font-weight: bold;
  display: block;
  margin-left: 10px;
  margin-top: 5px;
`
const ProfileEmailHeader = styled.span`
  width: 100%;
  height: 20px;
  font-size: 13px;
  display: block;
  margin-left: 10px;
  margin-top: 2px;
`

/* - - - RawNav Components - - - */

const RawNavbar = styled.div`
  width: 100%;
  height: auto;
  direction: ltr;
  ul {
    width: 100%;
    height: auto;
    padding-left: 0px;
    list-style: none;
    overflow: hidden;
  }
  li[class="line"] {
    border-bottom: 1px solid #999;
  }
  li {
    width: 100%;
    height: 30px;
    color: #333;
    line-height: 30px;
    margin-top: 5px;
    padding-bottom: 5px;
    cursor: pointer;
    svg {
      width: 20px;
      height: 20px;
      display: block;
      margin: 5px auto;
      padding-left: 10px;
      float: left;
    }
    a {
      width: 100px;
      height: 30px;
      display: block;
      float: left;
      line-height: 30px;
      font-size: 13px;
      padding-left: 10px;
      color: #333;
    }
  }
`
const RawNavImgbar = styled.img`
  width: 20px;
  height: 27px;
  float: left;
  margin-left: 10px;
`
