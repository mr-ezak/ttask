import styled from "styled-components"
import { Header } from "./Header"

const PanelLayout = props => (
  <Wrapper>
    <Header />
    {props.children}{" "}
  </Wrapper>
)

export default PanelLayout

const Wrapper = styled.div`
  overflow: hidden;
`
