import styled from "styled-components"
import * as Icons from "./Icons"

export const NewGroup = props => {
  return <NewGroupWraper top={props.top}></NewGroupWraper>
}

export const NewGroupBlank = props => {
  return <MainNewGroupBlank display={props.display} {...props} />
}

/* - - - Styled Components - - - */

/* - - - Main Styled Components - - - */
const NewGroupWraper = styled.div`
  width: 300px;
  height: 200px;
  position: fixed;
  margin-left: auto;
  margin-right: auto;
  left: 0;
  right: 0;
  top: ${p => (p.top ? p.top : 100)}px;
  z-index: 2;
  background: #f9f8eb;
  border-radius: 5px;
  box-shadow: 0px 0px 5px #999;
  transition: all 500ms;
`

const MainNewGroupBlank = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.5);
  z-index: 1;
  top: 0;
  display: ${p => (p.display ? p.display : "none")};
  transition: all 500ms;
`
