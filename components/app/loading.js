import styled from "styled-components"
import { LoadingIcon } from "./Icons"

const Loading = props => (
  <LandingWrapper>
    <LoadingIcon />
    <span>Loading . . .</span>
  </LandingWrapper>
)

export default Loading

const LandingWrapper = styled.div`
  width: 200px;
  height: 200px;
  margin: 100px auto;
  overflow: hidden;
  svg {
    width: 150px;
    height: 150px;
    margin: 0px auto;
    display: block;
  }
  span {
    width: 100%;
    text-align: center;
    display: block;
    font-family: Montserrat;
    font-size: 16px;
    color: #333;
  }
`
